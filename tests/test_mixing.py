import numpy as np
import episcen.mixing as mx
import pytest


def test_isolated():
    num_strata = 3
    mixmat = np.eye(num_strata)
    for ix in range(num_strata):
        beta_i = np.zeros(num_strata)
        beta_i[ix] = 1
        force = mx.force_of_infection(beta_i, mixmat)
        assert np.array_equal(force, beta_i)


def test_isolated_2():
    mixmat = np.array([[0, 0, 1], [0, 1, 0], [1, 0, 0]])
    beta_i = np.array([1, 0, 0])
    force = mx.force_of_infection(beta_i, mixmat)
    assert np.array_equal(force, np.array([0, 0, 1]))


def test_mixing_1():
    mixmat = np.array([[0.5, 0.25, 0.25], [0, 0.5, 0.5], [0.4, 0, 0.6]])
    beta_i = np.array([2, 2, 2])
    force = mx.force_of_infection(beta_i, mixmat)
    assert np.isclose(np.sum(beta_i), np.sum(force))
    for ix in range(mixmat.shape[0]):
        assert np.isclose(force[ix], 2 * np.sum(mixmat[:, ix]))


def calc_contact_matrix():
    """Return the contact matrix for a population of 18 strata."""
    # The population distribution across 18 strata.
    pi = np.array(
        [
            0.007690,
            0.006210,
            0.006310,
            0.003890,
            0.003640,
            0.002790,
            0.001620,
            0.000640,
            0.000220,
            0.123600,
            0.102770,
            0.152130,
            0.138140,
            0.131440,
            0.123530,
            0.099880,
            0.059310,
            0.036190,
        ]
    )
    n_strata = len(pi)

    # Start with a homogeneous mixing matrix, where each ROW m[r] represents
    # the (fractional) distribution of contacts for ONE STRATUM.
    m = np.repeat([pi], n_strata, axis=0)
    # Ensure that each row m[r] sums to one.
    row_sums = np.sum(m, axis=1)
    assert np.allclose(1, row_sums)
    # Ensure that each column m[:, c], when divided by population, sums to the
    # same total.
    col_sums = np.sum(m, axis=0) / pi
    assert np.allclose(col_sums[0], col_sums)

    # Add inhomogeneous mixing, by increasing the proportion of mixing that
    # occurs between the Indigenous strata (rows 0:8).
    ix_mix = np.array(range(9))
    ix_rest = np.array([i for i in range(n_strata) if i not in ix_mix])
    propn_mix = 0.8
    for ix in ix_mix:
        m[ix, ix_mix] = propn_mix / len(ix_mix)
        denom = np.sum(m[ix, ix_rest])
        m[ix, ix_rest] = m[ix, ix_rest] * (1 - propn_mix) / denom

    # Construct the absolute mixing matrix, which characterises the contact
    # rates between each pair of strata.
    num_contacts = 20
    kappa = num_contacts * m * 365

    return kappa


def calc_R0_scaling_multi(R0_values):
    """
    Construct the model parameters that are required to calculate the R0
    scaling factor for multiple R0 values.
    """
    kappa = calc_contact_matrix()
    n_strata = kappa.shape[0]

    # The transmission probability matrix: given contact between susceptible
    # and infectious individuals, define the probability of transmission.
    # NOTE: when modifying this, must carefully consider the orientation
    # (rows versus columns).
    tpm = np.ones(kappa.shape)

    mask_172 = np.isclose(R0_values, 1.72)
    # NOTE: assume both periods are 1 day for R0 = 1.72.
    gamma_1 = 365 * np.ones((len(R0_values), n_strata))
    gamma_2 = 365 * np.ones((len(R0_values), n_strata))
    # NOTE: assume longer periods for R0 != 1.72
    inf_period = 1 / 0.1302
    inf_period_I1 = 2.0
    inf_period_I2 = inf_period - inf_period_I1
    gamma_1[~mask_172] = 365 / inf_period_I1
    gamma_2[~mask_172] = 365 / inf_period_I2

    return mx.R0_scaling_multi(R0_values, tpm, kappa, gamma_1, gamma_2)


def calc_R0_scaling(R0_val, n_samples):
    """
    Construct the model parameters that are required to calculate the R0
    scaling factor.
    """
    kappa = calc_contact_matrix()
    n_strata = kappa.shape[0]

    # The transmission probability matrix: given contact between susceptible
    # and infectious individuals, define the probability of transmission.
    # NOTE: when modifying this, must carefully consider the orientation
    # (rows versus columns).
    tpm = np.ones(kappa.shape)

    R0 = R0_val * np.ones(n_samples)
    if np.allclose(R0, 1.72):
        # NOTE: different periods are assumed for R0 = 1.72.
        gamma_1 = 365 * np.ones(n_strata)
        gamma_2 = 365 * np.ones(n_strata)
    else:
        inf_period = 1 / 0.1302
        inf_period_I1 = 2.0
        inf_period_I2 = inf_period - inf_period_I1
        gamma_1 = 365 / inf_period_I1 * np.ones(n_strata)
        gamma_2 = 365 / inf_period_I2 * np.ones(n_strata)

    # Check that we obtain the same value from both R0_scaling functions.
    v1 = mx.R0_scaling(R0, tpm, kappa, gamma_1, gamma_2)
    v2 = mx.R0_scaling_multi(R0, tpm, kappa, gamma_1, gamma_2)
    assert np.allclose(v1, v2)

    return v1


@pytest.fixture(params=[1.72, 2.38, 2.68, 2.98])
def R0_value(request):
    """A simple fixture that iterates over R0 values."""
    return request.param


def expected_R0_scaling():
    """Return the R0 scaling factors as calculated by our Matlab model."""
    # for these four R0 values.
    return {
        2.38: 0.01549380,
        2.68: 0.01744680,
        2.98: 0.01939980,
        1.72: 0.04300000,
    }


def test_R0_scaling(R0_value):
    """
    Test that the R0 scaling factor is consistent with our Matlab model.
    """
    expected = expected_R0_scaling()
    assert R0_value in expected
    n_samples = 5
    values = calc_R0_scaling(R0_value, n_samples)
    # Ensure we received a scaling factor for each R0 value.
    assert values.shape == (n_samples,)
    # Ensure each scaling factor has the expected value.
    assert np.allclose(values, expected[R0_value])


def test_R0_scaling_multi():
    """
    Test that the R0 scaling factor is consistent with our Matlab model.
    """
    expected = expected_R0_scaling()
    # Construct an array that contains each unique R0 value.
    R0_values = np.array([value for value in expected])
    values = calc_R0_scaling_multi(R0_values)
    # Ensure we received a scaling factor for each R0 value.
    assert values.shape == R0_values.shape
    # Ensure each scaling factor has the expected value.
    for R0_val, R0_scale in zip(R0_values, values):
        assert np.isclose(R0_scale, expected[R0_val])
