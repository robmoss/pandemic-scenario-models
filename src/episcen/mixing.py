import numpy as np
from numpy.linalg import eigvals


def force_of_infection(beta_i, mixmat):
    """
    Calculate the force of infection exerted on each population stratum, given
    the force of infection exerted by the infectious individuals in each
    stratum (``beta_i``) and the mixing matrix (``mixmat``).
    """
    return beta_i @ mixmat


def R0_scaling(R0, tpm, kappa, gamma_1, gamma_2):
    """
    Calculate the R0 scaling factor for N strata.

    :param R0: The basic reproduction number (scalar).
    :param tpm: The transmission probability matrix (N x N).
    :param kappa: The contact rate matrix (N x N).
    :param gamma_1: The rate parameter for I1 (N).
    :param gamma_2: The rate parameter for I2 (N).

    :returns: The R0 scaling factor (scalar).
    """
    net_inf_period = 1 / gamma_1 + 1 / gamma_2
    net_inf_rate = 1 / net_inf_period
    ngm = kappa * tpm / net_inf_rate
    m_eig = np.real(np.max(eigvals(ngm)))
    return R0 / m_eig


def R0_scaling_multi(R0, tpm, kappa, gamma_1, gamma_2):
    """
    Calculate the R0 scaling factor for any number of R0 values and N strata.

    :param R0: The basic reproduction number (V).
    :param tpm: The transmission probability matrix (N x N).
    :param kappa: The contact rate matrix (N x N).
    :param gamma_1: The rate parameter for I1 (V, N).
    :param gamma_2: The rate parameter for I2 (V, N).

    :returns: The R0 scaling factor (V).
    """
    net_inf_period = 1 / gamma_1 + 1 / gamma_2
    net_inf_rate = 1 / net_inf_period
    # NOTE: need to add an extra singleton dimension to net_inf_rate.
    ngm = kappa * tpm / net_inf_rate[..., None]
    # NOTE: calculate maximum values over the final dimension (N).
    m_eig = np.real(np.max(eigvals(ngm), axis=-1))
    return R0 / m_eig
