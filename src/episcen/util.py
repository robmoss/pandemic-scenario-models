import numpy as np
import numpy.lib.recfunctions
import scipy.stats


def repack(svec, astype=float):
    """
    Return a copy of the array ``svec`` where the fields are contiguous and
    viewed as a regular Numpy array of floats.
    """
    out = numpy.lib.recfunctions.repack_fields(svec).view(astype)
    # NOTE: reshape to retain the columns as a dimension.
    new_shape = (*svec.shape, len(svec.dtype.fields), -1)
    return out.reshape(new_shape)


def outflow(n, p, rnd):
    """
    Sample the number of individuals that leave each compartment.

    :param n: The number of people in each compartment.
    :type n: numpy.ndarray
    :param p: The probability that an individual will leave each compartment.
    :type p: numpy.ndarray
    :param rnd: A source of random numbers.
    :type rnd: numpy.random.Generator

    :rtype: numpy.ndarray
    """
    # Determine the output shape, as determined by broadcasting the numbers of
    # people (n) and the individual probabilities (p) against each other.
    shape = np.broadcast(n, p).shape

    # Create the outflow array, and broadcast the input arrays as required.
    out = np.zeros(shape, dtype=int)
    n = np.broadcast_to(n, shape)
    p = np.broadcast_to(p, shape)

    # Identify where non-zero samples may occur.
    mask_nz = np.logical_and(n >= 1, p > 0)

    # Sample from Poisson distributions where we have at least 1000 people and
    # the individual probability is 1 in 10,000 or less (law of rare events).
    cond_poiss = np.logical_and(n >= 1000, p <= 1e-4)
    # cond_poiss = n >= 1000
    mask_poiss = np.logical_and(mask_nz, cond_poiss)
    mask_binom = np.logical_and(mask_nz, ~cond_poiss)

    # Draw binomial samples where required.
    if np.any(mask_binom):
        dist = scipy.stats.binom(n=n[mask_binom], p=p[mask_binom])
        out[mask_binom] = dist.rvs(random_state=rnd)

    # Draw Poisson samples where required.
    if np.any(mask_poiss):
        dist = scipy.stats.poisson(mu=n[mask_poiss] * p[mask_poiss])
        out[mask_poiss] = dist.rvs(random_state=rnd)

    return out


def split_outflow(n, p, rnd):
    """
    Split the individuals that leave each compartment into two flows.

    :param n: The number of people leaving each compartment.
    :type n: numpy.ndarray
    :param p: The probability that an individual will be selected for the
        first flow out of each compartment.
    :type p: numpy.ndarray
    :param rnd: A source of random numbers.
    :type rnd: numpy.random.Generator

    :rtype: (numpy.ndarray, numpy.ndarray)
    """
    out_a = outflow(n, p, rnd)
    return (out_a, n - out_a)
