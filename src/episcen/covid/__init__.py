"""
A simple scenario model for COVID-19 in Australia.
"""

import h5py
import importlib.resources as res
import io
import numpy as np
import os.path
import pypfilt
import pypfilt.stats
import scipy.stats

from .. import mixing as mx
from ..util import outflow, split_outflow


def scenarios():
    """
    Return the COVID-19 scenario definitions.

    :rtype: io.StringIO

    :Examples:

    >>> import pypfilt
    >>> import episcen.covid
    >>> instances = list(pypfilt.load_instances(episcen.covid.scenarios()))
    >>> print(len(instances))
    4
    >>> ids = set(instance.scenario_id for instance in instances)
    >>> print(ids == {'COVID_AU_Baseline', 'COVID_AU_Full',
    ...               'COVID_AU_Dist1', 'COVID_AU_Dist2'})
    True
    """
    return io.StringIO(res.read_text('episcen.covid', 'scenarios.toml'))


def calc_contact_matrix(pi):
    """
    Return the contact matrix for a stratified population.

    :param pi: An array that defines the fraction of the population that
        belongs to each stratum.
    """
    n_strata = len(pi)

    # Start with a homogeneous mixing matrix, where each ROW m[r] represents
    # the (fractional) distribution of contacts for ONE STRATUM.
    m = np.repeat([pi], n_strata, axis=0)
    # Ensure that each row m[r] sums to one.
    row_sums = np.sum(m, axis=1)
    assert np.allclose(1, row_sums)
    # Ensure that each column m[:, c], when divided by population, sums to the
    # same total.
    col_sums = np.sum(m, axis=0) / pi
    assert np.allclose(col_sums[0], col_sums)

    # Add inhomogeneous mixing, by increasing the proportion of mixing that
    # occurs between the Indigenous strata (rows 0:8).
    ix_mix = np.array(range(9))
    ix_rest = np.array([i for i in range(n_strata) if i not in ix_mix])
    propn_mix = 0.8
    for ix in ix_mix:
        m[ix, ix_mix] = propn_mix / len(ix_mix)
        denom = np.sum(m[ix, ix_rest])
        m[ix, ix_rest] = m[ix, ix_rest] * (1 - propn_mix) / denom

    # Construct the absolute mixing matrix, which characterises the contact
    # rates between each pair of strata.
    num_contacts = 20
    kappa = num_contacts * m * 1

    return kappa


def dist_alpha_m(eta):
    """
    Construct the prior distribution for :math:`\\alpha_m`.

    :param eta: The case-severity parameter :math:`\\eta`.
    """
    # Scale eta to span the interval [0.1, 10]
    eta = 100 * eta
    # Scale eta to span the interval [0.2, 1.0]
    scale = (eta - 0.1) * 0.8 / 9.9 + 0.2
    # The mean spans [0.1, 0.5]
    mmean = scale * 2 / 4
    # The minimum spans [0.05, 0.25]
    minv = scale * 1 / 4
    # The maximum spans [0.15, 0.75]
    maxv = scale * 3 / 4
    # The variance spans [0.02, 0.1]
    varn = scale * 1 / 10

    # Define the translation and scaling.
    span = maxv - minv
    scaled_mean = (mmean - minv) / span
    scaled_var = varn / span

    # Calculate the shape parameters.
    a = np.square(scaled_mean / scaled_var) * (1 - scaled_mean) - scaled_mean
    b = a * (1 - scaled_mean) / scaled_mean

    dist = scipy.stats.beta(a=a, b=b, loc=minv, scale=span)
    return dist.ppf


def dependent_dists(indep_values, dep_params):
    """
    The dependent distributions function for the COVID-19 scenario model.
    """
    return {
        'alpha_m': {
            'ppf': dist_alpha_m(indep_values['eta']),
        }
    }


class Model(pypfilt.Model):
    r"""
    The Australian COVID-19 scenario model.

    The population is divided into :math:`N_S` strata.
    The state vector contains the following fields:

    - Model parameters (floats):

      - "eta" (scalar): The proportion of infections that present with severe
        symptoms.
      - "alpha_m" (scalar): The proportion of infections that present with
        mild symptoms.
      - "R0" (scalar): The basic reproduction number.
      - "R0_scale" (scalar): The scaling factor for R0, which accounts for the
        transmission probability and contact rate matrices.

    - Infection compartments (integers):

      - "S" :math:`(N_S)`: The susceptible population.
      - "E1" :math:`(N_S)`: The early incubation period.
      - "E2" :math:`(N_S)`: The late incubation period.
      - "I1" :math:`(N_S)`: The early infectious period.
      - "I2" :math:`(N_S)`: The late infectious period.
      - "R" :math:`(N_S)`: The recovered population.
      - "M" :math:`(N_S)`: Managed cases, ascertained upon leaving "I1".
      - "RM" :math:`(N_S)`: The recovered population of managed cases.

    - Contact-tracing compartments (floats):

      - "CT_m" :math:`(N_S)`
      - "CT_nm" :math:`(N_S)`

    - Quarantine compartments (integers):

      - "E1q" :math:`(N_S)`: The early incubation period (quarantined).
      - "E2q" :math:`(N_S)`: The late incubation period (quarantined).
      - "I1q" :math:`(N_S)`: The early infectious period (quarantined).
      - "I2q" :math:`(N_S)`: The late infectious period (quarantined).
      - "Rq" :math:`(N_S)`: The recovered population (quarantined).
      - "Mq" :math:`(N_S)`: Managed cases (quarantined).
      - "RMq" :math:`(N_S)`: Recovered managed cases (quarantined).

    - Event counters (integers):

      - "Cum_Out_I1" :math:`(N_S)`: The number of people who have left "I1".
      - "Cum_Out_I1q" :math:`(N_S)`: The number of people who have left "I1q".
      - "Cum_Managed" :math:`(N_S)`: The number of people who have entered "M"
        or "Mq".
      - "Cum_Mild" :math:`(N_S)`: The number of people who left "I1" or "I1q",
        and have mild symptoms.
      - "Cum_Severe" :math:`(N_S)`: The number of people who left "I1" or
        "I1q", and require hospitalisation.
      - "Cum_ICU" :math:`(N_S)`: The number of people who left "I1" or
        "I1q", and require ICU admission.
    """

    def __init__(self):
        self.pi = None

    def init(self, ctx, vec):
        model_params = ctx.settings['model']

        self.popn_size = model_params['population_size']
        self.pi = np.array(model_params['pi'])
        self.ns = self.popn_size * self.pi
        self.kappa = calc_contact_matrix(self.pi)
        self.ascertain_frac = model_params['ascertainment']
        self.ascertain_imp = model_params['ascertainment_impact']
        self.quarantine_imp = model_params['quarantine_impact']
        self.quarantine_frac = model_params['quarantine_proportion']
        self.delta = 1 / model_params['contact_tracing_duration']
        self.import_rate = model_params['imports_per_week'] / 7

        self.propn_icu = model_params['propn_icu']
        # Scale the age-specific hospitalisation proportions so that they take
        # their maximal values when eta = 0.1.
        propn_hosp = model_params['propn_hosp_by_age']
        self.eta_relscale = 10 * np.tile(propn_hosp, 2)

        # Incubation period of 3.2 days.
        incub_period = 3.2
        incub_period_E2 = 1.6
        incub_period_E1 = incub_period - incub_period_E2
        self.sigma_1 = 1 / incub_period_E1
        self.sigma_2 = 1 / incub_period_E2

        # Infectious period of 7.68 days.
        inf_period = 9.68
        inf_period_I1 = 4.0
        inf_period_I2 = inf_period - inf_period_I1
        self.gamma_1 = 1 / inf_period_I1
        self.gamma_2 = 1 / inf_period_I2

        # Assume that self-quarantined cases will be identified in only 1 day,
        # rather than in 2 days.
        inf_period_I1q = 4.0
        inf_period_I2q = inf_period - inf_period_I1
        self.gamma_1q = 1 / inf_period_I1q
        self.gamma_2q = 1 / inf_period_I2q

        prior = ctx.data['prior']

        vec['R0'] = prior['R0']
        tpm = np.ones(self.kappa.shape)
        vec['R0_scale'] = mx.R0_scaling_multi(
            vec['R0'],
            tpm,
            self.kappa,
            np.array(self.gamma_1)[None],
            np.array(self.gamma_2)[None],
        )

        init_exps = 20
        E1_init = np.round(init_exps * 0.5 * self.pi)
        E2_init = np.round(init_exps * 0.5 * self.pi)
        vec['S'] = self.popn_size * self.pi - E1_init - E2_init
        vec['E1'] = E1_init
        vec['E2'] = E2_init
        vec['eta'] = prior['eta']
        vec['alpha_m'] = prior['alpha_m']

    def __dtype(self, num_strata):
        strata_shape = (num_strata,)
        return [
            # Model parameters.
            ('eta', float),
            ('alpha_m', float),
            ('R0', float),
            ('R0_scale', float),
            # Baseline model compartments.
            ('S', int, strata_shape),
            ('E1', int, strata_shape),
            ('E2', int, strata_shape),
            ('I1', int, strata_shape),
            ('I2', int, strata_shape),
            ('R', int, strata_shape),
            ('M', int, strata_shape),
            ('RM', int, strata_shape),
            # Contract-tracing compartments.
            ('CT_m', float, strata_shape),
            ('CT_nm', float, strata_shape),
            # Quarantine compartments.
            ('E1q', int, strata_shape),
            ('E2q', int, strata_shape),
            ('I1q', int, strata_shape),
            ('I2q', int, strata_shape),
            ('Rq', int, strata_shape),
            ('Mq', int, strata_shape),
            ('RMq', int, strata_shape),
            # Counters for different case types.
            ('Cum_Out_I1', int, strata_shape),
            ('Cum_Out_I1q', int, strata_shape),
            ('Cum_Managed', int, strata_shape),
            ('Cum_Mild', int, strata_shape),
            ('Cum_Severe', int, strata_shape),
            ('Cum_ICU', int, strata_shape),
        ]

    def field_types(self, ctx):
        num_strata = len(ctx.settings['model']['pi'])
        return self.__dtype(num_strata)

    def population_size(self):
        return self.popn_size

    def update(self, ctx, time_step, is_fs, prev, curr):
        dt = time_step.dt
        ns = self.ns
        rnd = ctx.component['random']['model']

        curr[:] = prev[:]

        beta = prev['R0_scale'][:, None, None] * self.kappa
        # Transpose the beta matrix for each simulation.
        beta = np.transpose(beta, [0, 2, 1])

        # Calculate the effective number of infectious individuals, where
        # those in quarantine or isolation exert a lesser force of infection
        # than an infectious individual who is not quarantined or isolated.
        contrib_inf = (
            prev['I1']
            + prev['I2']
            + prev['M'] * (1 - self.ascertain_imp)
            + prev['I1q'] * (1 - self.quarantine_imp)
            + prev['I2q'] * (1 - self.quarantine_imp)
            + prev['Mq'] * (1 - max(self.quarantine_imp, self.ascertain_imp))
        )
        # Calculate the force of importation on each population group.
        force_import = self.import_rate * self.pi
        # The net force of infection is the combination of importations and
        # the daily contacts of infectious individuals in the community.
        force_inf = force_import + np.squeeze(beta @ contrib_inf[..., None])

        # The probability of an individual leaving each compartment.
        pr_out_S = dt * force_inf / ns
        pr_out_E1 = dt * self.sigma_1
        pr_out_E2 = dt * self.sigma_2
        pr_out_I1 = dt * self.gamma_1
        pr_out_I2 = dt * self.gamma_2
        pr_out_M = dt * self.gamma_2
        pr_out_E1q = dt * self.sigma_1
        pr_out_E2q = dt * self.sigma_2
        pr_out_I1q = dt * self.gamma_1q
        pr_out_I2q = dt * self.gamma_2q
        pr_out_Mq = dt * self.gamma_2q

        # Sample the number of individuals that leave each compartment.
        # NOTE: we use the provided PRNG to generate these samples.
        out_S = outflow(prev['S'], pr_out_S, rnd)
        out_E1 = outflow(prev['E1'], pr_out_E1, rnd)
        out_E2 = outflow(prev['E2'], pr_out_E2, rnd)
        out_I1 = outflow(prev['I1'], pr_out_I1, rnd)
        out_I2 = outflow(prev['I2'], pr_out_I2, rnd)
        out_M = outflow(prev['M'], pr_out_M, rnd)
        out_E1q = outflow(prev['E1q'], pr_out_E1q, rnd)
        out_E2q = outflow(prev['E2q'], pr_out_E2q, rnd)
        out_I1q = outflow(prev['I1q'], pr_out_I1q, rnd)
        out_I2q = outflow(prev['I2q'], pr_out_I2q, rnd)
        out_Mq = outflow(prev['Mq'], pr_out_Mq, rnd)

        # Determine the proportion of infected individuals that present when
        # they leave I1 or I1q.
        eta = prev['eta'][..., None] * self.eta_relscale[None, ...]
        alpha = eta + prev['alpha_m'][..., None] * (1 - eta)

        # Account for case ascertainment (I1 -> M).
        pr_asc = alpha * self.ascertain_frac
        out_I1_to_M, out_I1_to_I2 = split_outflow(out_I1, pr_asc, rnd)
        # Account for ascertainment of quarantined contacts.
        out_I1q_to_Mq, out_I1q_to_I2q = split_outflow(out_I1q, pr_asc, rnd)

        # Calculate flows for the two types of contacts:
        # - Those who are contacts of cases that were ascertained and are
        #   managed (CT_m);
        # - Those who are contacts of cases that are not managed (CT_nm).
        net_to_M = out_I1_to_M + out_I1q_to_Mq
        net_to_I2 = out_I1_to_I2 + out_I1q_to_I2q
        CT_m_in = np.squeeze(self.kappa.T @ net_to_M[..., None])
        CT_nm_in = np.squeeze(self.kappa.T @ net_to_I2[..., None])

        CT_denom = prev['CT_m'] + prev['CT_nm']
        denom_zero = CT_denom == 0
        with np.errstate(divide='ignore', invalid='ignore'):
            Theta_m = (prev['S'] / ns) * prev['CT_m'] / CT_denom
            Theta_nm = (prev['S'] / ns) * prev['CT_nm'] / CT_denom
        Theta_m[denom_zero] = 0
        Theta_nm[denom_zero] = 0

        # We could draw samples instead of always using these mean values.
        CT_m_out = self.delta * prev['CT_m'] + force_inf * Theta_m
        CT_nm_out = self.delta * prev['CT_nm'] + force_inf * Theta_nm
        # NOTE: without clipping, it's possible to obtain negative values.
        CT_m_out = np.clip(CT_m_out, 0, prev['CT_m'])
        CT_nm_out = np.clip(CT_nm_out, 0, prev['CT_nm'])

        # NOTE: distribute the new exposures to E1 and E1q in proportion to
        # Theta_nm (E1) and Theta_m (E1q).
        # A fraction of a managed case's contacts will self-quarantine.
        # All other exposed individuals will not self-quarantine.
        Theta_denom = Theta_m + Theta_nm
        with np.errstate(divide='ignore', invalid='ignore'):
            pr_to_E1q = self.quarantine_frac * Theta_m / Theta_denom
        pr_to_E1q[Theta_denom == 0] = 0
        out_S_E1q, out_S_E1 = split_outflow(out_S, pr_to_E1q, rnd)

        # Update the state of each compartment.
        curr['S'] = prev['S'] - out_S
        curr['E1'] = prev['E1'] + out_S_E1 - out_E1
        curr['E2'] = prev['E2'] + out_E1 - out_E2
        curr['I1'] = prev['I1'] + out_E2 - out_I1
        curr['I2'] = prev['I2'] + out_I1_to_I2 - out_I2
        curr['M'] = prev['M'] + out_I1_to_M - out_M
        curr['R'] = prev['R'] + out_I2
        curr['RM'] = prev['RM'] + out_M
        curr['CT_m'] = prev['CT_m'] + CT_m_in - CT_m_out
        curr['CT_nm'] = prev['CT_nm'] + CT_nm_in - CT_nm_out
        curr['E1q'] = prev['E1q'] + out_S_E1q - out_E1q
        curr['E2q'] = prev['E2q'] + out_E1q - out_E2q
        curr['I1q'] = prev['I1q'] + out_E2q - out_I1q
        curr['I2q'] = prev['I2q'] + out_I1q_to_I2q - out_I2q
        curr['Mq'] = prev['Mq'] + out_I1q_to_Mq - out_Mq
        curr['Rq'] = prev['Rq'] + out_I2q
        curr['RMq'] = prev['RMq'] + out_Mq

        # Sample clinical outcomes for each person leaving I1 or I1q
        net_I1x_out = out_I1 + out_I1q
        severe, non_severe = split_outflow(net_I1x_out, eta, rnd)
        icu = outflow(severe, self.propn_icu, rnd)
        mild = outflow(non_severe, prev['alpha_m'][..., None], rnd)

        # Update the cumulative totals.
        curr['Cum_Out_I1'] += out_I1
        curr['Cum_Out_I1q'] += out_I1q
        curr['Cum_Managed'] += out_I1_to_M + out_I1q_to_Mq
        curr['Cum_Mild'] += mild
        curr['Cum_Severe'] += severe
        curr['Cum_ICU'] += icu

        # Ensure that invariant conditions are satisfied.
        for field in curr.dtype.names:
            if np.any(curr[field] < 0):
                msg = 'ERROR: field "{}" is negative at time {}'
                print(msg.format(field, time_step.end))
                import pdb

                pdb.set_trace()

    def pr_inf(self, prev, curr):
        return -1

    def is_seeded(self, hist):
        return np.ones(hist['S'].shape, dtype=bool)


class AttackRates(pypfilt.Table):
    """
    A summary table that calculates the attack rate for each counter in the
    state vector (i.e., for fields whose name begins with ``"Cum_"``).
    """

    def field_types(self, ctx, obs_list, name):
        model = ctx.component['model']
        self.__fields = [
            field
            for field in model.field_names(ctx)
            if field.startswith('Cum_')
        ]
        time = pypfilt.io.time_field('time')
        attack_rate_fields = [
            (name.replace('Cum_', 'AR_'), float) for name in self.__fields
        ]
        return [
            time,
            ('ix', int),
            ('R0', float),
            ('eta', float),
            ('alpha_m', float),
        ] + attack_rate_fields

    def n_rows(self, ctx, forecasting):
        return ctx.particle_count()

    def add_rows(self, ctx, fs_time, window, insert_fn):
        particles = ctx.particle_count()
        popn_size = ctx.settings['model']['population_size']
        end_time = ctx.settings['time']['sim_until']

        for snapshot in window:
            if snapshot.time != end_time:
                continue
            svec = snapshot.state_vec
            sums = [np.sum(svec[name], axis=1) for name in self.__fields]
            sums = np.stack(sums, axis=-1)
            for px in range(particles):
                row_values = sums[px] / popn_size
                insert_fn(
                    (
                        snapshot.time,
                        px,
                        svec['R0'][px],
                        svec['eta'][px],
                        svec['alpha_m'][px],
                        *row_values,
                    )
                )


class DailyCases(pypfilt.Table):
    """
    A summary table that calculates the daily increase for each counter in the
    state vector (i.e., for fields whose name begins with ``"Cum_"``).
    """

    def field_types(self, ctx, obs_list, name):
        model = ctx.component['model']
        self.__all_obs = obs_list
        self.__prev_sums = None
        self.__fields = [
            field
            for field in model.field_names(ctx)
            if field.startswith('Cum_')
        ]
        time = pypfilt.io.time_field('time')
        incidence_fields = [
            (name.replace('Cum_', 'Daily_'), float) for name in self.__fields
        ]
        return [time, ('ix', int)] + incidence_fields

    def n_rows(self, ctx, forecasting):
        self.__prev_sums = None
        particles = ctx.particle_count()
        n_times = ctx.summary_count()
        return n_times * particles

    def add_rows(self, ctx, fs_time, window, insert_fn):
        particles = ctx.particle_count()

        for snapshot in window:
            svec = snapshot.state_vec
            sums = [np.sum(svec[name], axis=1) for name in self.__fields]
            sums = np.stack(sums, axis=-1)
            if self.__prev_sums is None:
                diffs = sums
            else:
                diffs = sums - self.__prev_sums
            for px in range(particles):
                row_values = diffs[px]
                insert_fn((snapshot.time, px, *row_values))
            self.__prev_sums = sums


def plot_attack_rates(png_file='COVID_AU_attack_rates.png'):
    """
    Plot the attack rates for each scenario.

    .. note:: You must run the scenario simulations before calling this
       function.

    :param png_file: The PNG file to which the plot will be saved.

    :raise FileNotFoundError: if the scenario output files do not exist.
    """
    # Define the scenario label for each scenario output file.
    scenarios = {
        'Unmitigated': 'COVID_AU_Baseline.hdf5',
        'Q + I': 'COVID_AU_Full.hdf5',
        'Dist 25%': 'COVID_AU_Dist1.hdf5',
        'Dist 33%': 'COVID_AU_Dist2.hdf5',
    }

    # Load the attack rate table for each scenario.
    time_scale = pypfilt.Scalar()
    dataset_path = '/tables/attack_rates'
    attack_rate_tables = []
    for _label, input_file in scenarios.items():
        if not os.path.exists(input_file):
            raise FileNotFoundError(input_file)
        with h5py.File(input_file, 'r') as f:
            dataset = f[dataset_path]
            table = pypfilt.io.load_dataset(time_scale, dataset)
            attack_rate_tables.append(table)

    # Generate plots non-interactively with the 'Agg' backend.
    import matplotlib

    matplotlib.use('Agg')
    import matplotlib.pyplot as plt

    # Plot the results in a 2x2 grid.
    rows = 2
    cols = 2
    titles = ['Clinical', 'ICU', 'Hospital', 'Total']
    fig, axs = plt.subplots(nrows=rows, ncols=cols, sharex=True)

    # Define the x-axis and the quantiles to plot.
    x_values = [1, 2, 3, 4]
    x_labels = list(scenarios.keys())
    probs = [0.05, 0.50, 0.95]

    for rix in range(rows):
        for cix in range(cols):
            ax = axs[rix, cix]

            # Define the sub-plot title.
            ix = cols * rix + cix
            title = titles[ix]
            ax.set_title(title)

            # Define the x-axis and y-axis settings
            ax.set_xticks(x_values, x_labels)
            ax.set_xlim(0.5, 4.5)
            if cix == 0:
                ax.set_ylabel('Attack Rate (%)')

            # Calculate the quantiles to plot for this sub-plot.
            y_medians = []
            y_lower = []
            y_upper = []
            for table in attack_rate_tables:
                if title == 'Clinical':
                    values = table['AR_Mild'] + table['AR_Severe']
                elif title == 'ICU':
                    values = table['AR_ICU']
                elif title == 'Hospital':
                    values = table['AR_Severe']
                elif title == 'Total':
                    values = table['AR_Out_I1'] + table['AR_Out_I1q']
                weights = np.ones(values.shape)
                (low, med, upp) = pypfilt.stats.qtl_wt(
                    100 * values, weights, probs
                )
                y_medians.append(med)
                # NOTE: errors are relative to the central value!
                y_lower.append(med - low)
                y_upper.append(upp - med)

            # Plot the quantiles as error bars.
            ax.errorbar(
                x_values,
                y_medians,
                [y_lower, y_upper],
                linestyle='',
                marker='o',
            )

    # Define the figure size (in inches).
    fig.set_figwidth(10)
    fig.set_figheight(6)

    fig.savefig(png_file, format='png', dpi=150, bbox_inches='tight')
