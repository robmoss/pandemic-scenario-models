import epifx.cmd.forecast

from . import scenarios, plot_attack_rates


def main():
    """
    Run the COVID-19 scenarios.
    """
    toml_data = scenarios().read()
    scenario_file = 'COVID_AU.toml'
    with open(scenario_file, 'w') as f:
        f.write(toml_data)

    epifx.cmd.forecast.main(['--spawn', '4', scenario_file])
    plot_attack_rates()


main()
