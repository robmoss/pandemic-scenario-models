Next version
------------

* Provide an implementation of our Australian COVID-19 scenario model (doi:10.3201/eid2612.202530).

0.1.0 (2020-12-07)
------------------

* Initial release.
