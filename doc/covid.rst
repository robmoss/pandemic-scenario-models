.. _covid:

Australian COVID-19 scenario model
==================================

This is an implementation of the disease transmission model described in the paper *Coronavirus Disease Model to Inform Transmission Reducing Measures and Health System Preparedness, Australia* (`doi:10.3201/eid2612.202530 <https://doi.org/10.3201/eid2612.202530>`__).

The following scenarios are predefined by :func:`episcen.covid.scenarios`:

"Unmitigated" (COVID_AU_Baseline)
    Epidemics produced in the absence of interventions.

"Q + I" (COVID_AU_Full)
    Epidemics produced subject to case-targeted interventions (quarantine and isolation).

"Dist 25%" (COVID_AU_Dist1)
    Epidemics produced subject to case-targeted interventions (as above) and social distancing measures that reduce transmission by 25%.

"Dist 33%" (COVID_AU_Dist2)
    Epidemics produced subject to case-targeted interventions (as above) and social distancing measures that reduce transmission by 33%.

.. figure:: COVID_AU_attack_rates.png
   :width: 100%

   Attack rates obtained from each of the four Australian COVID-19 scenarios.
   Points show median outcomes, and lines show the 5%–95% percentiles.

You can run the scenario simulations and produce the above plot with the following command:

.. code-block:: shell

    python3 -m episcen.covid

Main functions
--------------

.. autofunction:: episcen.covid.scenarios

.. autofunction:: episcen.covid.plot_attack_rates

Simulation components
---------------------

.. autoclass:: episcen.covid.Model

.. autoclass:: episcen.covid.AttackRates

.. autoclass:: episcen.covid.DailyCases

Internal functions
------------------

.. autofunction:: episcen.covid.dependent_dists

.. autofunction:: episcen.covid.dist_alpha_m

.. autofunction:: episcen.covid.calc_contact_matrix

.. autofunction:: episcen.util.outflow

.. autofunction:: episcen.util.split_outflow
