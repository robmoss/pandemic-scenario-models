Epidemic models for pandemic scenario analyses
==============================================

Welcome to the episcen_ documentation.
This package provides epidemic models designed for scenario modelling analyses, in order to inform pandemic preparedness and response activities.

License
-------

The code is distributed under the terms of the BSD 3-Clause license (see
``LICENSE``), and the documentation is distributed under the terms of the
`Creative Commons BY-SA 4.0 license
<http://creativecommons.org/licenses/by-sa/4.0/>`_.

.. _user-docs:

.. toctree::
   :maxdepth: 2
   :caption: User Documentation

   Home <self>
   install
   covid
   changelog
