Epidemic models for pandemic scenario analyses
==============================================

|version| |docs|

Description
-----------

A collection of epidemic models designed for scenario modelling analyses, in order to inform pandemic preparedness and response activities.

License
-------

The code is distributed under the terms of the `BSD 3-Clause license <https://opensource.org/licenses/BSD-3-Clause>`_ (see
``LICENSE``), and the documentation is distributed under the terms of the
`Creative Commons BY-SA 4.0 license
<http://creativecommons.org/licenses/by-sa/4.0/>`_.

Installation
------------

To install the latest release::

    pip install episcen

To install the latest development version, clone this repository and run::

    pip install .

.. |version| image:: https://badge.fury.io/py/episcen.svg
   :alt: Latest version
   :target: https://pypi.org/project/episcen/

.. |docs| image::  https://readthedocs.org/projects/episcen/badge/
   :alt: Documentation
   :target: https://episcen.readthedocs.io/
